'use strict';

angular.module('myApp.view1', ['ngRoute'])

.controller('View1Ctrl', ["$http", "$scope", "$uibModal", function($http, $scope, $uibModal) {
  var host = "https://mighty-sierra-54058.herokuapp.com";
  $scope.data = {
    value: 50,
    config: {}
  };

  $scope.mapObject = {
    scope: "est",
    options: {
      width: 1110,
      //legendHeight: 60 // optionally set the padding for the legend

    },
    bubblesConfig: {
      highlightBorderWidth: 0
    },
    geographyConfig: {
      highlightBorderColor: '#EAA9A8',
      highlightBorderWidth: 2,
      popupOnHover: false,
      highlightOnHover: false,
      dataUrl: '../visualization/d3/est.topo.json'
    },
    fills: {
      'HIGH': '#CC4731',
      'MEDIUM': '#306596',
      'LOW': '#667FAF',
      'defaultFill': '#DDDDDD'
    },
    data: {
    },
    setProjection: function (element) {
      var projection = d3.geo.mercator()
          .center([25.0136, 58.5953])
          .translate([element.offsetWidth / 2, element.offsetHeight / 2])
          .scale(7000)
          .rotate([0, 0, 0]);
      var path = d3.geo.path().projection(projection);
      return { path: path, projection: projection };
    }
  };

  $scope.mapPlugins = {
    bubbles: null
  }

  $scope.mapPluginData = {
    bubbles: []
  };

  $scope.bubbleClicked = function(bubble){
    var modalInstance = $uibModal.open({
      animation: true,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'dataModal.html',
      controller: 'DataModalCtrl',
      controllerAs: '$ctrl',
      size: 'lg',
      resolve: {
        data: function () {
          return $http.get(host + "/api/devices/" + bubble.id).then(function(response) {
            return response.data.data;
          });

        },
        bubble: function() {
          return bubble;
        }
      }
    });
  };

  $http.get(host + "/api/devices").then(function(response) {
    var bubbles = [];
    response.data.data.forEach(function(device) {
      device.radius = 10;
      device.fillKey = 'HIGH';
      bubbles.push(device)
    });
    $scope.mapPluginData.bubbles = bubbles;
  });
}]);

