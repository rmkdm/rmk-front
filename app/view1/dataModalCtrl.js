angular.module('myApp').controller('DataModalCtrl', ["$uibModalInstance", "data", "bubble", function ($uibModalInstance, data, bubble) {
    var $ctrl = this;
    $ctrl.bubble = bubble;
    $ctrl.data = data;
    $ctrl.ok = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);