angular.module('animatedLineGraph', [])
    .directive('d3AnimatedLineGraph', ['$window', '$timeout',
        function($window, $timeout) {
            return {
                restrict: 'EA',
                scope: {
                    data: '=',
                    label: '@',
                    onClick: '&',
                    mireName: '='
                },
                link: function(scope, ele, attrs) {
                        var renderTimeout;
                        var svg
                        var margin = parseInt(attrs.margin) || 80;
                        var width = ele[0].parentNode.offsetWidth - margin * 2;
                        var height = 500 - margin * 2;
                        var format = d3.time.format("%Y-%m-%dT%H:%M:%S.%L%L");

                        $window.onresize = function() {
                            scope.$apply();
                        };

                        scope.$watch(function() {
                            return angular.element($window)[0].innerWidth;
                        }, function() {
                            scope.render(scope.data);
                        });

                        scope.$watch('data', function(newData) {
                            scope.render(newData);
                        }, true);

                        scope.render = function(d) {
                            if (svg != null) svg.selectAll('*').remove();
                            if (!d) return;
                            if (renderTimeout) clearTimeout(renderTimeout);

                            renderTimeout = $timeout(function() {
                                width = ele[0].parentNode.offsetWidth - margin * 2;
                                // Scales and axes. Note the inverted domain for the y-scale: bigger is up!
                                var x = d3.time.scale().range([0, width]);
                                var y = d3.scale.linear().range([height, 0]);
                                var xAxis = d3.svg.axis().scale(x).tickSize(-height).tickSubdivide(true);
                                var yAxis = d3.svg.axis().scale(y).ticks(8).orient("right");

                                // An area generator, for the light fill.
                                var area = d3.svg.area()
                                    .interpolate("monotone")
                                    .x(function(d) { return x(d.timestamp); })
                                    .y0(height)
                                    .y1(function(d) { return y(d.water_level); });

                                // A line generator, for the dark stroke.
                                var line = d3.svg.line()
                                    .interpolate("monotone")
                                    .x(function(d) { return x(d.timestamp); })
                                    .y(function(d) { return y(d.water_level); });



                                d3.select(ele[0]).selectAll('*').remove();

                                var data = d.map(function(item){
                                    var e = item;
                                    if (typeof e.timestamp === 'string'){
                                        e.timestamp = format.parse(item.timestamp);
                                        e.water_level = item.water_level * 100
                                    }
                                    return e;
                                });
                                // Compute the minimum and maximum date, and the maximum level.
                                x.domain([data[0].timestamp, data[data.length - 1].timestamp]);
                                y.domain([
                                    d3.min(data, function(d) { return d.water_level; }),
                                    d3.max(data, function(d) { return d.water_level; })
                                ]).nice();

                                svg = d3.select(ele[0])
                                    .append('svg')
                                    .attr("width", width + margin * 2)
                                    .attr("height", height + margin * 2)
                                    .append("g")
                                    .attr("transform", "translate(" + margin + "," + margin+ ")");

                                // Add the title
                                svg.append("text")
                                    .attr("x", (width / 2))
                                    .attr("y", 0 - (margin / 2))
                                    .attr("text-anchor", "middle")
                                    .style("font-size", "16px")
                                    .style("text-decoration", "underline")
                                    .text("Water levels: " + data[0].timestamp.toDateString() + " - " + data[data.length -1].timestamp.toDateString());

                                // Add the clip path.
                                svg.append("clipPath")
                                    .attr("id", "clip")
                                    .append("rect")
                                    .attr("width", width)
                                    .attr("height", height);

                                // Add the x-axis.
                                svg.append("g")
                                    .attr("class", "x axis")
                                    .attr("transform", "translate(0," + height + ")")
                                    .call(xAxis)
                                    .selectAll("text")
                                    .style("text-anchor", "end")
                                    .attr("transform", "rotate(-65)");

                                // Add the y-axis.
                                svg.append("g")
                                    .attr("class", "y axis")
                                    .attr("transform", "translate(" + width + ",0)")
                                    .call(yAxis);

                                svg.append("text")
                                    .attr("transform", "rotate(-90)")
                                    .attr("y",width + margin / 2)
                                    .attr("x",0 - (height / 2))
                                    .attr("dy", "1em")
                                    .style("text-anchor", "middle")
                                    .text("Water level with respect to ground level. (cm)");

                                // zero line
                                svg.append('line')
                                    .style('stroke', '#F00')
                                    .style('stroke-width', '1px')
                                    .attr('x1', 0)
                                    .attr('y1', y(0))
                                    .attr('x2', width)
                                    .attr('y2', y(0));

                                // rework date
                                function isSubstring(str, substr) {
                                    return str.toLowerCase().indexOf(substr) !== -1;
                                }
                                function reworkDateByName(name) {
                                    if (isSubstring(name, "valgeraba")) {
                                        return new Date(2017, 0, 1);
                                    } else if (isSubstring(name, "riisa")) {
                                        return new Date(2017, 0, 1);
                                    } else if (isSubstring(name, "öördi")) {
                                        return new Date(2017, 0, 1);
                                    } else if (isSubstring(name, "linnusaare")) {
                                        return new Date(2016, 0, 1);
                                    } else if (isSubstring(name, "hara")) {
                                        return new Date(2014, 0, 1);
                                    } else if (isSubstring(name, "viru")) {
                                        return new Date(2013, 0, 1);
                                    } else if (isSubstring(name, "rannu")) {
                                        return new Date(2014, 0, 1);
                                    } else if (isSubstring(name, "endla")) {
                                        return new Date(2015, 0, 1);
                                    } else if (isSubstring(name, "sinilaugas")) {
                                        return new Date(2015, 0, 1);
                                    }
                                    return false;
                                }
                                function dateIsInRange(d) {
                                    return data[0].timestamp.getTime() <= d.getTime() && data[data.length - 1].timestamp.getTime() >= d.getTime();
                                }

                                var reworkDate = reworkDateByName(scope.mireName);
                                if (reworkDate && dateIsInRange(reworkDate)) {
                                    svg.append('line')
                                        .style('stroke', '#0F0')
                                        .style('stroke-width', '1px')
                                        .attr('x1', x(reworkDate))
                                        .attr('y1', 0)
                                        .attr('x2', x(reworkDate))
                                        .attr('y2', height);

                                    svg.append("text")
                                        .attr("y", height / 2 )
                                        .attr("x", x(reworkDate))
                                        .attr('text-anchor', 'middle')
                                        .attr("class", "myLabel")//easy to style with CSS
                                        .attr("transform", "rotate(-90, " + x(reworkDate) +", " + height / 2 + ")")
                                        .text("Start of the mire restoration works");
                                }

                                var colors = d3.scale.category10();
                                svg.selectAll('.line')
                                    .data([data]) //Should insert more values here for more lines
                                    .enter()
                                    .append('path')
                                    .attr('class', 'line')
                                    .style('stroke', function(d) {
                                        return colors(Math.random() * 50);
                                    })
                                    .attr('clip-path', 'url(#clip)')
                                    .attr('d', function(d) {
                                        return line(d);
                                    });

                                /* Add 'curtain' rectangle to hide entire graph */
                                var curtain = svg.append('rect')
                                    .attr('x', -1 * width)
                                    .attr('y', -1 * height)
                                    .attr('height', height)
                                    .attr('width', width)
                                    .attr('class', 'curtain')
                                    .attr('transform', 'rotate(180)')
                                    .style('fill', '#ffffff')

                                /* Optionally add a guideline */
                                var guideline = svg.append('line')
                                    .attr('stroke', '#333')
                                    .attr('stroke-width', 0)
                                    .attr('class', 'guide')
                                    .attr('x1', 1)
                                    .attr('y1', 1)
                                    .attr('x2', 1)
                                    .attr('y2', height);

                                /* Add hover functionality */

                                var focus = svg.append("g")
                                    .attr("class", "focus")
                                    .style("display", "none");

                                focus.append("circle")
                                    .attr("r", 4.5);

                                focus.append("text")
                                    .attr("x", 9)
                                    .attr("dy", ".35em");

                                svg.append("rect")
                                    .attr("class", "overlay")
                                    .attr("width", width)
                                    .attr("height", height)
                                    .on("mouseover", function() { focus.style("display", null); })
                                    .on("mouseout", function() { focus.style("display", "none"); })
                                    .on("mousemove", mousemove);

                                var bisectDate = d3.bisector(function(d) { return d.timestamp; }).left;

                                var dateLabel = focus.select("text")
                                    .append('tspan')
                                    .attr("x", 10);

                                var waterLabel = focus.select("text")
                                    .append('tspan')
                                    .attr("dy", 20)
                                    .attr("x", 10);
                                
                                function mousemove() {
                                    var x0 = x.invert(d3.mouse(this)[0]),
                                        i = bisectDate(data, x0, 1),
                                        d0 = data[i - 1],
                                        d1 = data[i],
                                        d = x0 - d0.timestamp > d1.timestamp - x0 ? d1 : d0;
                                    focus.attr("transform", "translate(" + x(d.timestamp) + "," + y(d.water_level) + ")");
                                    dateLabel.text(d.timestamp.toDateString());
                                    waterLabel.text("Lvl: " + d.water_level.toFixed(2) + "cm");
                                }


                                /* Create a shared transition for anything we're animating */
                                var t = svg.transition()
                                    .delay(750)
                                    .duration(6000)
                                    .ease('linear')
                                    .each('end', function() {
                                        d3.select('line.guide')
                                            .transition()
                                            .style('opacity', 0)
                                            .remove()
                                    });

                                t.select('rect.curtain')
                                    .attr('width', 0);
                                t.select('line.guide')
                                    .attr('transform', 'translate(' + width + ', 0)')

                                d3.select("#show_guideline").on("change", function(e) {
                                    guideline.attr('stroke-width', this.checked ? 1 : 0);
                                    curtain.attr("opacity", this.checked ? 0.75 : 1);
                                })



                            }, 200);
                        };

                }}
        }])