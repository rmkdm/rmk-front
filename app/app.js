'use strict';

// Declare app level module which depends on views, and components
var app = angular.module('myApp', [
    'ngRoute',
    'myApp.view1',
    'ui.bootstrap',
    'datamaps',
    'animatedLineGraph'

]).config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/home', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        })
        .otherwise({redirectTo: '/home'});
}]);

app.controller('MainController', ['$scope', '$location', function($scope, $location) {
    $scope.navClass = function (page) {
        var currentRoute = $location.path().substring(1) || 'home';
        return page === currentRoute ? 'active' : '';
    };
}]);