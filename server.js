var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.json());

// Create link to Angular build directory
var distDir = __dirname + "/app/";
app.use(express.static(distDir));

console.log("Dist");
console.log(distDir);
app.get('/',function(req,res){
    res.sendFile(path.join(distDir+'/index.html'));
});

var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});